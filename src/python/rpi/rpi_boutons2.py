#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Source Rpi Inputs: 
https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

try:
    import RPi.GPIO as GPIO
except ImportError:
    import GPIOEmu as GPIO
import time


class RpiBoutons():
    SEQ_LIMITE = 10

    def __init__(self, cmd, cmd_next):
        self._cmd = cmd
        self._next_cmd = cmd_next
        self._btn_list_appuie = dict()
        self._btn_list_relache = dict()
        self._dict_seq = dict()
        self._list_seq = list()
        self._max_seq = self.SEQ_LIMITE

    def charge_config(self, btn_list_appuie: dict =dict(), btn_relache: dict=dict()):
        # Use the Broadcom SOC Pin numbers
        # Setup the Pin with Internal pullups enabled and PIN in reading mode.
        GPIO.setmode(GPIO.BCM)

        # Buttons appuyés
        for btn, values in btn_list_appuie.items():
            GPIO.setup(int(btn), GPIO.IN, pull_up_down=GPIO.PUD_UP)

            self._btn_list_appuie[int(btn)] = values

        # Buttons relachés
        for btn, values in btn_relache.items():
            GPIO.setup(int(btn), GPIO.IN, pull_up_down=GPIO.PUD_UP)

            self._btn_list_relache[int(btn)] = values

    def run(self):
        '''
        Boucle d'exécution afin d'attendre d'éventuel appuie boutons
        '''
        last_val_appuie = self._lect_valeurs(self._btn_list_appuie)
        last_val_relache = self._lect_valeurs(self._btn_list_relache)

        try:
            while True:
                if not self._detect_exec(self._btn_list_appuie, last_val_appuie, False):
                    self._detect_exec(self._btn_list_relache,
                                      last_val_relache, True)
                time.sleep(0.005)

        except KeyboardInterrupt:
            GPIO.cleanup()

    def set_list_seq(self, list_seq_btn):
        self._list_seq = list_seq_btn

    # Our function on what to do when the button is pressed
    def _bouton_exec(self, channel, cmd_val):
        if isinstance( cmd_val, str):
            if cmd_val.upper() in "NEXT":
                self._next_cmd()
        else:
            self._cmd(cmd_val)
        print("channel : %s" % str(channel))
        self._update_channel_seq(channel)

    def _update_channel_seq(self, channel):
        for seq in self._list_seq:
            seq.event(channel)

    def _recherche_sequence(self):
        for seq in self._dict_seq:
            list_sequence = list(seq)
            list_out = [i for i, j in zip(
                list_sequence, self._list_seq) if i == j]
            if len(list_sequence) == len(list_out):
                # Appel fonction
                self._dict_seq[seq]()
                # Vide la sequence qui vient d'etre traité
                del self._list_seq[:]

    def _lect_valeurs(self, btn_list):
        last_val = dict()
        for ch_id in btn_list:
            last_val[ch_id] = GPIO.input(ch_id)
        return last_val

    def _detect_exec(self, btn_list, last_val, etat_attendu):
        '''
        Lit valeur et arrete dès qu'un bouton est détecté
        :param btn_list:
        :param last_val:
        :param method_btn:
        '''
        return_val = False

        for ch_id in btn_list:
            valeur_lue = GPIO.input(ch_id)

            # Sauve nouvel valeur avant comparaison de détection d'appuie sur
            # boutons
            last_ch_val = last_val[ch_id]
            last_val[ch_id] = valeur_lue

            if valeur_lue == etat_attendu and valeur_lue != last_ch_val:
                self._bouton_exec(ch_id, btn_list[ch_id]["cmd_val"])
                print('Input was {0}'.format(valeur_lue))
                return_val = True
                break

        return return_val
