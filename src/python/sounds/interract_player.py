#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


class InterractPlayer(object):
    def __init__(self, interact):
        self._interact = interact

    def set_commande(self, cmd):
        pass

    @staticmethod
    def get_command_interact(interact, action=None):
        cmd = InterractPlayer(interact)
        cmd.configure_action(action)
        return cmd

    def play(self):
        print(self._method.__func__.__name__)
        self._method()

    def configure_action(self, action):
        if action in "NEXT":
            self._method = self._interact.play_next_no_seq
        elif action in "NEXT_CONFIG":
            self._method = self._interact.change_next_config
        else:
            RuntimeError("ca n'existe pas")
