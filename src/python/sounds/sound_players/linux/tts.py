#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import subprocess


class TTS(object):
    '''
    Utilise le module de sortie audio depuis un message texte
    '''

    def __init__(self, message):
        self._message = message
        self._cmd = ["espeak", "-vfr", self._message]

    def play(self):
        self._cmd[len(self._cmd) - 1] = self._message
        print(" ".join(self._cmd))
        player = subprocess.Popen(
            self._cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _ = player.communicate()
#         print(output)

    def set_msg(self, message):
        self._message = message


if __name__ == "__main__":
    msg = "coucou"
    print(msg)
    try:
        sound = TTS(msg)
        sound.play()
    except RuntimeError as e:
        pass
    finally:
        print(msg)
    exit(0)
