#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Speak user generated text.
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os
import subprocess


class Sound(object):

    def __init__(self, message):
        self._message = message
        #self._cmd = ["mplayer", "file://" + self._message]
        self._cmd = ["mpg321", "file://" + self._message]
        self._cmd = ["aplay", self._message]

    def play(self):
        print(" ".join(self._cmd))
        player = subprocess.Popen(
            self._cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = player.communicate()
#         print(output)

    def set_msg(self, message):
        self._message = message


if __name__ == "__main__":
    msg = "coucou"
    chemin_fichier = "file://" + os.path.join(
        os.getcwd(),  "..", "..", "wav", "1_minute.wav")
    print(chemin_fichier)
    try:
        sound = Sound(chemin_fichier)
        sound.play()
    except RuntimeError as e:
        pass
    finally:
        print(chemin_fichier)
    exit(0)
