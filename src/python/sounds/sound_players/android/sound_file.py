#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Speak user generated text.
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os

try:
    import sl4a as andro
    
except ImportError as e:
    try:
        import androidhelper as andro
    except ImportError as e:

        raise
#     import sl4a_fake as sl4a


class Sound(object):

    def __init__(self, message):
        self._droid = andro.Android()
        self._message = "file://" + message

    def play(self):
        print('Android play :{0}'.format(self._message))
        self._droid.mediaPlay(self._message)

    def set_msg(self, message):
        pass


if __name__ == "__main__":
    msg = "coucou"
    chemin_fichier = os.path.join(os.getcwd(), '..', 'wav', "1_minute.wav")
    print(chemin_fichier)
    try:
        sound = Sound(chemin_fichier)
        sound.play()
    except RuntimeError as e:
        pass
    finally:
        print(chemin_fichier)
    exit(0)
