#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Speak user generated text.
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

# try:
#import sl4a
import androidhelper as andro
# except ImportError as e:
#     import sl4a_fake as sl4a


class TTS(object):

    def __init__(self, message):
        self._droid = andro.Android()
        self._message = message

    def play(self):
        print('Android play tts :{0}'.format(self._message))
        self._droid.ttsSpeak(self._message)

    def set_msg(self, message):
        self._message = message


if __name__ == "__main__":
    msg = "coucou"
    print(msg)
    try:
        sound = TTS(msg)
        sound.play()
    except RuntimeError as e:
        pass
    finally:
        print(msg)
    exit(0)
