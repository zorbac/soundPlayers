#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

print("Android import failed")
import sys
from types import ModuleType
mymodule = ModuleType('sl4a')


class Android(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass

    def mediaPlay(self, message):
        print(message)

    def ttsSpeak(self, message):
        print(message)

    def set_msg(self, message):
        self._message = message


mymodule.__setattr__("Android", Android)
sys.modules["sl4a"] = mymodule
