#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

try:
    from sounds.sound_players.android import sound_file, tts
except:
    try:
        from sounds.sound_players.linux_termux import sound_file, tts
    except Exception as e:
        print(e)
        from sounds.sound_players.linux import sound_file, tts

import os

import sounds.sound_players.console.affiche_text as affiche_text


class SoundPlayerTypes():
    '''
    Types pour la factory
    '''
    SOUND_FILE = "FILE"
    CONSOLE = "CONSOLE"
    SOUND_TTS = "TTS"


class SoundPlayerFactory(object):
    '''
    Factory de creation des modules de sons
    '''

    def __init__(self):
        pass

    def creer_player(self, nom_player, parametre):

        if nom_player == SoundPlayerTypes.SOUND_TTS:
            player = tts.TTS(parametre)
        elif nom_player == SoundPlayerTypes.SOUND_FILE:
            player = sound_file.Sound(parametre)
        elif nom_player == SoundPlayerTypes.CONSOLE:
            player = affiche_text.AfficheText(parametre)
        else:
            player = affiche_text.AfficheText(parametre)
        return player


if __name__ == '__main__':
    dossier_courant_fichier = os.path.dirname(os.path.abspath(__file__))
    chemin_fichier = os.path.join(
        dossier_courant_fichier,  "..", "wav", "1_minute.wav")
    play = SoundPlayerFactory().creer_player(
        SoundPlayerTypes.SOUND_FILE, chemin_fichier)
    play.play()

    play = SoundPlayerFactory().creer_player(
        SoundPlayerTypes.SOUND_TTS, "bonjour")
    play.play()

    play = SoundPlayerFactory().creer_player(
        SoundPlayerTypes.CONSOLE, "bonjour")
    play.play()
