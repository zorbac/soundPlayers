#-*-coding:utf8;-*-
#qpy:3
#qpy:webapp:sound player web Qpython
#qpy://127.0.0.1:8081/
'''
@author: zorbac

This is based on sample for qpython webapp
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import bottle


try:
    from web import services
    from web.server import MyWSGIRefServer
    from sounds.sound_interraction import LoaderInterraction
except Exception as e:
    print(e)

__author__ = 'zorbac at free.fr'
__copyright__ = 'WTPL'
__license__ = 'WTPL'

######### QPYTHON WEB SERVER ###############

interact = LoaderInterraction.get_interraction(True)

services.set_interraction(interact)

app = bottle.default_app()

try:
    import socket

    hostname = socket.gethostname()
    if hostname in "raspberrypi":
        server = MyWSGIRefServer(host="192.168.254.195",  port="8081")
    else:
        server = MyWSGIRefServer(port="8081")
#     server.quiet = True
    app.run(server=server, reloader=False)
except (Exception) as ex:
    print("Exception: %s" % repr(ex))
