#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import sys
import os
import subprocess
sys.path.append("..")
sys.path.append(os.path.dirname(__file__))


from sounds.sound_interraction import LoaderInterraction
from rpi.configuration_rpi import ConfigurationRpi
from sounds.sound_players import sound_player_factory
from common.configuration_loader import ConfigurationLoader
from sequences.command_player import CommandPlayer
from sequences.sequence import Sequence


class Sequence1(object):
    def __init__(self):
        self._list_id = list()
        self._msg = ''
        self._cpt_seq = 1
        self._cpt_seq_ec = 0
        self._msg_final = ''
        self._action = None
        self._player = None
        self._nom = None

    def set_nom(self, nom):
        self._nom = nom

    def set_seq(self, json_seq):
        self._config = json_seq
        if 'sequence' in json_seq:
            self._list_id_events = json_seq['sequence']
            self._taille_list = len(self._list_id_events)
        if 'message' in json_seq:
            self._msg = json_seq['message']
            self._msg_final = self._msg
        if 'cpt_seq' in json_seq:
            self._cpt_seq = json_seq['cpt_seq']

    def set_action_exec(self, action):
        self._action = action

    def set_player(self, player):
        self._player = player

    def event(self, new_id):
       # print(self._list_id)
        self._list_id.append(new_id)
        if len(self._list_id) > self._taille_list:
            del self._list_id[0]
        self._verif_event()
      #  print(self._list_id)

    def _verif_event(self):
        if self._list_id == self._list_id_events:
            self._cpt_seq_ec += 1
            activation = self._cpt_seq_ec % self._cpt_seq
            if activation == 0:
                msg = "détection séquence {0}, complété".format(self._msg)

            else:
                msg = "détection séquence {0}, reste {1}".format(
                    self._msg, activation)

            print(msg)

            self._player.set_msg(msg)
            self._player.play()
            if activation == 0:
              #  print("action : {}".format(self._action))
                if self._action is not None:
                    self._action.play()

            del self._list_id[:]


def obtenir_config():
    path_config = os.path.join(os.path.dirname(__file__),
                               #"..",
                               "configs")
    nom_config = "config_seq.json"
    gestion_config = ConfigurationLoader(
        path_config)
    gestion_config.set_configuration_file_name(nom_config)
    gestion_config.set_chemin_configuration_default(os.path.dirname(__file__))
    config_json = gestion_config.obtenir_configuration()
    return config_json


def lance():
    # Configuration
    path_config = os.path.join(os.path.dirname(__file__),
                               #"..",
                               "configs",
                               "config_seq.json")

#    rpi_config = ConfigurationRpi(path_config)
#    json_config_rpi = rpi_config.get_config()  # @UnusedVariable
#    print(json_config_rpi)

    config = ConfigurationLoader(path_config)
#    json_config = config.obtenir_configuration()
    json_config = obtenir_config()
    print(json_config)

    # Module pour le son
    interact = LoaderInterraction.get_interraction()

   # print(json_config_rpi["sequences"])

    player_seq = sound_player_factory.SoundPlayerFactory()\
        .creer_player(json_config["TYPE"],
                      'bonjour')
#    player_seq = sound_player_factory.SoundPlayerFactory()\
#                .creer_player(sound_player_factory.SoundPlayerTypes.SOUND_TTS,
#                              'bonjoir')
    list_seq = list()
    for nom_seq, detail_seq in json_config["sequences"].items():
        # print(nom_seq)
      #  print(detail_seq)
        seq = Sequence()
        seq.set_nom(nom_seq)
        detail_seq['sequence']
        seq.set_msg(detail_seq['message'])
        seq.set_cpt_seq(detail_seq['cpt_seq'])
        # print(type(detail_seq['sequence']))
        seq.set_seq(detail_seq['sequence'])
        if detail_seq['type'] in "SND":
            player = sound_player_factory.SoundPlayerFactory()\
                .creer_player(detail_seq['action']["TYPE"],
                              detail_seq['action']["FICHIER"])
        elif detail_seq['type'] in "CMD":
            player = CommandPlayer(detail_seq['action'][detail_seq['type']])
         #   print(detail_seq['action'][detail_seq['type']])
        seq.set_action_exec(player)
        seq.set_player(player_seq)
        list_seq.append(seq)

    for _ in range(2):
        pass
        for cpt in range(4):
            for seq in list_seq:
                print("nom : {}".format(seq._nom))
                seq.event(18 + cpt)
                pass
  #  for cpt in range(7):
   #     seq.event(18)

    list_seq = [18, 18, 18, 12]
    list_attendu = [18, 18, 18]
    len_verif = len(list_attendu)

   # print(list_seq[0:len_verif])
 #   seq.new_seq(list_seq)
    # if list_seq[0:len_verif] == list_attendu:
    #    print('uururjtit')

    #seq = SeqMsg()
    # seq.set_seq(json_config_rpi["sequence"])


if __name__ == "__main__":
    #     gestion_bouton()
    #     func_with_events()
    lance()
