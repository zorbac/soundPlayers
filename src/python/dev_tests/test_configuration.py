#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

from sounds.configuration import configuration
import os

if __name__ == '__main__':
    dossier_courant_fichier = os.path.dirname(os.path.abspath(__file__))
    gestion_config = configuration.Configuration(dossier_courant_fichier)
    gestion_config.set_configuration_file_name(
        "/mnt/1T/01_Documents_Secondaires/Programmation/PythonScripts/devSounds/configs/config_nintendo/config_nintendo.json")
    config_text = gestion_config.obtenir_configuration()
    print(config_text)
    for value in config_text:
        print(value)
