#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from sounds.sound_players import sound_player_factory
from common.configuration_loader import ConfigurationLoader
from sequences.command_player import CommandPlayer
from sequences.sequence import Sequence
from sounds.interract_player import InterractPlayer


def _obtenir_config_seq(dossier_config):
    nom_config = "config_seq.json"
    gestion_config = ConfigurationLoader(
        dossier_config)
    gestion_config.set_configuration_file_name(nom_config)
    gestion_config.set_chemin_configuration_default(os.path.dirname(__file__))
    config_json = gestion_config.obtenir_configuration()
    return config_json


def obtenir_list_seq(dossier_config, interact=None):

    json_config = _obtenir_config_seq(dossier_config)
#     print(json_config)

    snd_player_factory = sound_player_factory.SoundPlayerFactory()

    player_seq = snd_player_factory.creer_player(json_config["TYPE"],
                                                 'non defini')
    list_seq = list()
    for nom_seq, detail_seq in json_config["sequences"].items():
        # print(nom_seq)
      #  print(detail_seq)
        seq = Sequence()
        seq.set_nom(nom_seq)
#         detail_seq['sequence']
        seq.set_msg(detail_seq['message'])
        seq.set_cpt_seq(detail_seq['cpt_seq'])
        # print(type(detail_seq['sequence']))
        seq.set_seq(detail_seq['sequence'])

        if detail_seq['type'] in "SND":
            player = snd_player_factory.creer_player(detail_seq['action']["TYPE"],
                                                     detail_seq['action']["FICHIER"])
        elif detail_seq['type'] in "CMD":
            player = CommandPlayer(detail_seq['action'][detail_seq['type']])
         #   print(detail_seq['action'][detail_seq['type']])
        elif detail_seq['type'] in "INTERACT":
            #             print(detail_seq)
            if interact is not None:
                player = InterractPlayer.get_command_interact(
                    interact, detail_seq['action'])
            else:
                player = None
                print('Interaction non definie')

        seq.set_action_exec(player)
        seq.set_player(player_seq)
        list_seq.append(seq)
    return list_seq


def lance_test_seq():
    # Configuration
    path_config = os.path.join(os.path.dirname(__file__),
                               "..",
                               "configs", 'rpi')

    list_seq = obtenir_list_seq(path_config)

    for _ in range(2):
        pass
        for cpt in range(4):
            for seq in list_seq:
                print("nom : {}".format(seq._nom))
                seq.event(18 + cpt)
                pass
  #  for cpt in range(7):
   #     seq.event(18)


if __name__ == "__main__":
    lance_test_seq()
