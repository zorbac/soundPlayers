#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import subprocess


class CommandPlayer(object):
    def __init__(self, list_cmd=None):
        # test platform pour eviter extinction
        #         print("Command : {0}".format(list_cmd))
        self._cmd = list_cmd

    def set_commande(self, cmd):
        self._cmd = cmd

    def play(self):
        print(" ".join(self._cmd))
        process = subprocess.Popen(
            self._cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = process.communicate()
        print("commande sortie : {}".format(out))


def lance():
    cmd = CommandPlayer()
    cmd.set_commande(['ls', '/'])
    cmd.play()


if __name__ == "__main__":
    lance()
