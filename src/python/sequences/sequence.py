#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


class Sequence(object):
    def __init__(self):
        self._list_id = list()
        self._list_id_events = list()
        self._msg = ''
        self._cpt_seq = 1
        self._cpt_seq_ec = 0
        self._msg_final = ''
        self._action = None
        self._player = None
        self._nom = None
        self._taille_list = 0

    def set_nom(self, nom):
        self._nom = nom

    def set_msg(self, message):
        self._msg = message
        self._msg_final = self._msg

    def set_cpt_seq(self, somme):
        '''
        Prend en compte le nombre de détection de la séquence avant de l'enclancher
        Minimum 1
        :param somme:
        '''
        if somme <= 0:
            self._cpt_seq = 1
        else:
            self._cpt_seq = somme

    def set_seq(self, seq):
        #        self._config = json_seq
       # if 'sequence' in json_seq:
        self._list_id_events = seq
        self._taille_list = len(self._list_id_events)
        # if 'message' in json_seq:
     #   self._msg = json_seq['message']
      #  self._msg_final = self._msg
       # if 'cpt_seq' in json_seq:
        #    self._cpt_seq = json_seq['cpt_seq']

    def set_action_exec(self, action):
        self._action = action

    def set_player(self, player):
        self._player = player

    def event(self, new_id):
       # print(self._list_id)
        self._list_id.append(new_id)
        if len(self._list_id) > self._taille_list:
            del self._list_id[0]
        self._verif_event()
      #  print(self._list_id)

    def _verif_event(self):
        if self._list_id == self._list_id_events:
            self._cpt_seq_ec += 1
            activation = self._cpt_seq_ec % self._cpt_seq
            if activation == 0:
                msg = "détection séquence {0}, complété".format(self._msg)

            else:
                msg = "détection séquence {0}, reste {1}".format(
                    self._msg, activation)

            print(msg)

            self._player.set_msg(msg)
            self._player.play()
            if activation == 0:
              #  print("action : {}".format(self._action))
                if self._action is not None:
                    self._action.play()

            del self._list_id[:]


def lance():
    class NullPlayer(object):
        def __init__(self):
            pass

        def set_msg(self, msg):
            pass

        def play(self):
            pass
    seq = Sequence()
    player = NullPlayer()
    seq.set_player(player)
    seq.set_action_exec(player)
    seq.event(1)


if __name__ == "__main__":
    lance()
