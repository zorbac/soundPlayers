#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Fichiers contenant les liens de la page web
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

__author__ = 'zorbac at free.fr'
__copyright__ = 'WTPL'
__license__ = 'WTPL'


from bottle import Bottle, ServerAdapter
from bottle import run, debug, route, error, static_file, template
from bottle import request, get, post

import os

try:
    from sounds.sound_interraction import LoaderInterraction
except Exception as e:
    print(e)


interact = None

# ######### BUILT-IN ROUTERS ###############


@get('/__exit')
@post('/__exit')
def __exit():
    # print('fufuffuuti')
    global server
    server.stop()


@get('/__ping')
@post('/__ping')
def __ping():
    msg = '<a href="/">retour</a>'
    return msg + "    ok"


@get('/assets/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='/sdcard')


@get('/_play/<indd:int>')
def _play_sound(indd):
    global interact
    interact.play_sound(indd)
    return home()


@get('/_play_next')
def _play_next():
    global interact
    interact.play_next()
    return home()


@post('/changeconfig')
def change_config():
    global interact
    option = request.forms.get('options')
    print('option : {0}'.format(option))
    interact.set_new_config(option)
    return home()


@post('/reload_configs')
def reload_configs():
    global interact
    interact.reload_configs()
    return home()


@post('/next_config')
def next_config():
    global interact
    interact.change_next_config()
    return home()


######### WEBAPP ROUTERS ###############
@get('/')
def home():
    global interact
    return template(os.path.join((os.path.split(__file__))[0], 'ressources', 'page.tpl'),
                    name='QPython',
                    list_config=interact.get_configs_name(),
                    current_config=interact.get_current_config(),
                    nbre_sons=interact.get_number_sounds())

######### WEBAPP ROUTERS ###############


def set_interraction(p_interact):
    global interact
    interact = p_interact
