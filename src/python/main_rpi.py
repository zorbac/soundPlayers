#-*-coding:utf8;-*-
#qpy:3
'''
@author: zorbac

Lanceur principal pour le RaspberryPi
'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

from rpi.rpi_launcher import lance


if __name__ == "__main__":
    lance()
