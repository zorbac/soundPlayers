[![Build status](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://gitlab.com/zorbac/soundPlayers/commits/master)

# Sound Player
> Outil permettant de produire des sons sur Android et Linux via WSGI.<br>
> Utilisation possible avec RaspberryPi ou en console.<br>
> Configuration via fichiers JSON.

## Installation

Linux:
- [ ] __A FAIRE__



Android:

- [ ] __A FAIRE__
- Installer Termux-API pour accéder aux éléments Android
    https://wiki.termux.com/wiki/Termux:API

### Reglages Rpi
* Réglage du volume sonore</br>

```shell
amixer
amixer sset PCM 80%
``` 

* Services lancé au démarrage<br> 

  * programme réaction boutons connectés<br>

```shell
sudo nano /lib/systemd/system/sound_player.service
```

```ini
[Unit]
Description=Sound Interactions
After=multi-user.target
[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/pi/Hugo/soundPlayer/main_rpi.py
[Install]
WantedBy=multi-user.target
``` 

```shell
sudo chmod 644 /lib/systemd/system/sound_player.service
sudo systemctl daemon-reload
sudo systemctl enable sound_player.service
```


  * programme interface web<br>

```shell
sudo nano /lib/systemd/system/sound_player_web.service
```

```ini
[Unit]
Description=Sound Interactions Web
After=multi-user.target
[Service]
Type=idle
ExecStart=/usr/bin/python3 /home/pi/Hugo/soundPlayer/main_web.py
[Install]
WantedBy=multi-user.target
```

```shell
sudo chmod 644 /lib/systemd/system/sound_player_web.service
sudo systemctl daemon-reload
sudo systemctl enable sound_player_web.service
```

#### Schema cablage
 <img src="schema/cablage_rpi.png" alt="Cablage Rpi" width="800"> </img>
 

## Configuration

### Fichiers config son
...

## Usage example

- [ ] __A FAIRE__

## Development setup

Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

__A FAIRE__

## A faire

- [ ] Ajouter une zone de saisi de texte sur la page pour faire de la synthese sur n'importe quoi


## Historique versions

* 1 Premiere version
* 2 Sequence fonctionnelle pour les sons
    Ajout gestion NEXT Config son

## Meta

Zorbac

Distribué sous la licence WTFPL. Voir ``LICENSE`` pour plus d'information.

[https://gitlab.com/zorbac/soundPlayers](https://gitlab.com/zorbac/soundPlayers/)

## Contribution

Fork du projet

