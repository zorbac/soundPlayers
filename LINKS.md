
Liste de liens utilisés pour le développement.

### Programmation
#### Python

* [Jouer musiques depuis Python](https://raspberrypi.stackexchange.com/questions/7088/playing-audio-files-with-python)
  * http://stackoverflow.com/questions/17657103/how-to-play-wav-file-in-python
  * [Kivy extension](https://duckduckgo.com/?q=android+kivy+play+mp3&t=lm&atb=v55-5&ia=qa)
* [Qpython et SL4A](http://wiki.qpython.org/en/sl4a/)


##### Library WSGI
###### Bottle
* https://www.digitalocean.com/community/tutorials/how-to-use-the-bottle-micro-framework-to-develop-python-web-apps
* https://bottlepy.org/docs/dev/tutorial_app.html
* http://pwp.stevecassidy.net/bottle/forms-processing.html
* http://stackoverflow.com/questions/31662681/flask-handle-form-with-radio-buttons
* [Bottle REST](https://www.toptal.com/bottle/building-a-rest-api-with-bottle-framework) 

##### Astuces
* [Vider le buffer d'entrée clavier](http://stackoverflow.com/questions/28906571/how-to-clear-the-stdin-buffer-in-python-when-stdin-is-non-interactive)
* [Changer variable environnement](http://stackoverflow.com/questions/5971635/setting-reading-environment-variables#5971692)

#### Curses (gestion console)
* https://docs.python.org/2/library/curses.html

#### Git
* [Gestion probleme avec GIT](https://stackoverflow.com/questions/19474186/egit-rejected-non-fast-forward)

#### README/Markdown
* https://dbader.org/blog/write-a-great-readme-for-your-github-project
* https://docs.gitlab.com/ce/user/markdown.html

### Linux
#### TTS
* http://stackoverflow.com/questions/1614059/how-to-make-python-speak
  * Choix espeak -vfr bonjour
  * http://espeak.sourceforge.net/
  
### Android
* [Scripting](https://github.com/damonkohler/sl4a)
* https://github.com/damonkohler/sl4a/blob/wiki/ApiReference.md#mediaPlay.md
  * https://github.com/ainsophical/DROID_PYTHON/wiki/SL4A-MediaPlayerFacade


### Raspberry Pi
- [Player adapté](http://www.raspberrypi-spy.co.uk/2013/06/raspberry-pi-command-line-audio/)<br>
-- Choix aplay car les autre étaient trop lent avant de sortir le son<br>
-- [Audio Rpi](https://www.raspberrypi.org/documentation/usage/audio/)<br>
* [Gestion interrupteur](http://www.framboise314.fr/le-bouton-poussoir-un-composant-banal-o-combien-etonnant/#Programme_anti-rebond_logiciel) <br>
-- [LED and buttons](https://projects.drogon.net/raspberry-pi/gpio-examples/tux-crossing/3-more-leds-and-a-button/)<br>
-- [Button et switch](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/buttons_and_switches/)<br>
* Gestion Services au démarrage<br>
-- [Rpi SystemD](http://www.raspberrypi-spy.co.uk/2015/10/how-to-autorun-a-python-script-on-boot-using-systemd/)


### Web page
Formatage CSS afin d'aligner les éléments

* [CSS 3 colonnes](http://stackoverflow.com/questions/20566660/3-column-layout-html-css#20566735)
* [Table et CSS](https://css-tricks.com/complete-guide-table-element/)

## Musiques
### Chiptunes 8bits
* http://woolyss.com/chipmusic-samples.php
### Samples
* https://www.freesound.org
